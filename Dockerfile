FROM hulkinbrain/docker-opencv2

ENV DEBIAN_FRONTEND=noninteractive

USER root

RUN apt-get update &&\
    apt-get -y install git unzip build-essential wget

WORKDIR /build

RUN git clone https://github.com/sergiomsilva/alpr-unconstrained &&\
    cd alpr-unconstrained/ &&\
    cd darknet/ && make && cd .. &&\
    bash get-networks.sh

USER myuser

RUN pip install --user keras==2.2.4 tensorflow==1.5.0 numpy==1.14

RUN cd alpr-unconstrained/ && bash run.sh -i samples/test -o /tmp/output -c /tmp/output/results.csv

WORKDIR /build/alpr-unconstrained

ENTRYPOINT [ "./run.sh" ]
